<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Ptk')->middleware('cors')->group(function() {

    Route::get('ptk/{nip}/gaji', 'PtkGajiController@index');
    Route::get('ptk/{nip}/pangkat', 'PtkPangkatController@index');
    Route::get('ptk/{ptk}/sekolah', 'PtkSekolahController@index');
    Route::get('ptk/stats', 'PtkController@stats');
    Route::get('ptk/{ptk}', 'PtkController@show');
    Route::get('ptk', 'PtkController@index');
    Route::post('ptk', 'PtkController@store');
    Route::delete('ptk/{ptk}', 'PtkController@destroy');
    Route::patch('ptk/{ptk}', 'PtkController@update');

});

Route::get('/bidang-studi', 'BidangStudi\BidangStudiController@index');

Route::namespace('User')->middleware('cors')->group(function() {
    Route::post('user/login', 'UserController@login');
});

//Route::apiResource('/user', 'User\UserController', ['middleware' => 'cors']);
Route::get('/sekolah/stats/persentase', 'Sekolah\SekolahController@percentageStats');
Route::get('/sekolah/find', 'Sekolah\SekolahController@find');
Route::get('/sekolah/rombel', 'Rombel\RombelController@sekolah');


Route::apiResource('/kecamatan', 'Area\KecamatanController', ['middleware' => 'cors']);
Route::apiResource('/kota', 'Area\KotaController', ['middleware' => 'cors']);
Route::apiResource('/agama', 'Agama\AgamaController', ['middleware' => 'cors']);
Route::apiResource('/sekolah', 'Sekolah\SekolahController', ['middleware' => 'cors']);
Route::apiResource('/guru', 'Guru\GuruController', ['middleware' => 'cors']);
Route::apiResource('/berita', 'Berita\BeritaController', ['middleware' => 'cors']);
Route::apiResource('/operator', 'Operator\OperatorController', ['middleware' => 'cors']);
Route::apiResource('/jenis-pegawai', 'JenisPegawai\JenisPegawaiController', ['middleware' => 'cors']);
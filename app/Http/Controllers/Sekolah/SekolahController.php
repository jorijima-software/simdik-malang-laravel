<?php

namespace App\Http\Controllers\Sekolah;

use App\Http\Controllers\Controller;
use App\Http\Resources\Sekolah\Collection;
use App\Http\Resources\Sekolah\SekolahFindCollection;
use App\Http\Resources\Sekolah\SekolahResource;
use App\Sekolah;
use Illuminate\Http\Request;

class SekolahController extends Controller
{
    public function index()
    {
        return Collection::collection(Sekolah::take(10)->skip(request('start'))->get())->additional(
            ['total' => Sekolah::count()]
        );
    }

    public function percentageStats() {
        $sekolah = new Sekolah;
        return $sekolah->countPersentase();
    }

    public function find()
    {
        return SekolahFindCollection::collection(
            Sekolah::take(10)
                ->where('nama', 'like', '%' . request('search') . '%')
                ->orWhere('npsn', 'like', '%' . request('search') . '%')
                ->get()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sekolah $sekolah
     * @return \Illuminate\Http\Response
     */
    public function show(Sekolah $sekolah)
    {
        return new SekolahResource($sekolah);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sekolah $sekolah
     * @return \Illuminate\Http\Response
     */
    public function edit(Sekolah $sekolah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Sekolah $sekolah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sekolah $sekolah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sekolah $sekolah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sekolah $sekolah)
    {
        //
    }
}

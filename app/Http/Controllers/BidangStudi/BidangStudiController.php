<?php

namespace App\Http\Controllers\BidangStudi;

use App\Model\BidangStudi\BidangStudi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BidangStudiController extends Controller
{
    public function index()
    {
        return BidangStudi::take(10)
            ->where('nama', 'like', '%' . request('search') . '%')
            ->orWhere('kode', 'like', '%' . request('search') . '%')
            ->get();
    }
}

<?php

namespace App\Http\Controllers;

use App\TugasMapelGuru;
use Illuminate\Http\Request;

class TugasMapelGuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TugasMapelGuru  $tugasMapelGuru
     * @return \Illuminate\Http\Response
     */
    public function show(TugasMapelGuru $tugasMapelGuru)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TugasMapelGuru  $tugasMapelGuru
     * @return \Illuminate\Http\Response
     */
    public function edit(TugasMapelGuru $tugasMapelGuru)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TugasMapelGuru  $tugasMapelGuru
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TugasMapelGuru $tugasMapelGuru)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TugasMapelGuru  $tugasMapelGuru
     * @return \Illuminate\Http\Response
     */
    public function destroy(TugasMapelGuru $tugasMapelGuru)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use JWTFactory;

class UserController extends Controller
{
    public function login(Request $request) {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $username = request('username');
        $password = request('password');
        $level = request('level');

        if($user = User::where('username', $username)->first()) {
            $credential = [
                'username' => $username,
                'password' => $password,
                'level' => $level
            ];

            $token = null;
            try {
                if(!$token = JWTAuth::attempt($credential)) {
                    return response()->json([
                        'error' => 'Username or Password incorrect'
                    ]);
                }
            } catch (JWTException $e) {
                return response()->json([
                    'error' => 'FAILED_TO_CREATE_TOKEN_EXCEPTION'
                ], 500);
            }

            $payload =  [
                'username' => $user->username,
                'level' => $user->level,
                'sekolah' => !is_null($user->sekolah) ? $user->sekolah : null,
                'operator' => !is_null($user->sekolah) && !is_null($user->sekolah->operator) ? $user->sekolah->operator : null
            ];

            return response()->json([
                'success' => true,
                'token' => $token,
                'additionalToken' => base64_encode(json_encode($payload))
            ], 201);
        }

        return response()->json(['error' => 'USERNAME_NOT_FOUND_EXCEPTION'], 200);
    }

    public function index()
    {
        return User::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}

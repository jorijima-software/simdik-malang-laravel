<?php

namespace App\Http\Controllers;

use App\TingkatPendidikan;
use Illuminate\Http\Request;

class TingkatPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TingkatPendidikan  $tingkatPendidikan
     * @return \Illuminate\Http\Response
     */
    public function show(TingkatPendidikan $tingkatPendidikan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TingkatPendidikan  $tingkatPendidikan
     * @return \Illuminate\Http\Response
     */
    public function edit(TingkatPendidikan $tingkatPendidikan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TingkatPendidikan  $tingkatPendidikan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TingkatPendidikan $tingkatPendidikan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TingkatPendidikan  $tingkatPendidikan
     * @return \Illuminate\Http\Response
     */
    public function destroy(TingkatPendidikan $tingkatPendidikan)
    {
        //
    }
}

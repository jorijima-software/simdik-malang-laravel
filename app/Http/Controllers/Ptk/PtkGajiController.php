<?php

namespace App\Http\Controllers\Ptk;

use App\Http\Controllers\Controller;
use App\PtkGaji;
use Illuminate\Http\Request;

class PtkGajiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PtkGaji  $ptkGaji
     * @return \Illuminate\Http\Response
     */
    public function show(PtkGaji $ptkGaji)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PtkGaji  $ptkGaji
     * @return \Illuminate\Http\Response
     */
    public function edit(PtkGaji $ptkGaji)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PtkGaji  $ptkGaji
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PtkGaji $ptkGaji)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PtkGaji  $ptkGaji
     * @return \Illuminate\Http\Response
     */
    public function destroy(PtkGaji $ptkGaji)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Ptk;

use App\Http\Controllers\Controller;
use App\Http\Middleware\VerifyJWTToken;
use App\Http\Resources\Ptk\Collections;
use App\Http\Resources\Ptk\PtkCollection;
use App\Http\Resources\Ptk\PtkResource;
use App\Http\Resources\Ptk\PtkStatsResource;
use App\Ptk;
use Illuminate\Http\Request;
use JWTAuth;

class PtkController extends Controller
{
    public function __construct()
    {
        $this->middleware(VerifyJWTToken::class)->except('stats');
    }

    public function stats()
    {
        $ptk = new Ptk;
        $id = request('npsn');
        return [
            'kelengkapan' => round($ptk->persentase($id), 2)
        ];
    }

    public function index()
    {
        $user = JWTAuth::parseToken()->toUser();
        $ptk = new Ptk;

        $data = ($user->level == 0) ? $ptk->show(request()->all()) : Ptk::take(1);
        $total = $data->count();
        return PtkCollection::collection($data->skip(request('start'))->get())->additional(
            [
                'total' => $total,
                'user' => $user
            ]
        );
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = (object)request()->all();
//        $kepegawaian = (object)$request->kepegawaian;
//        $profil = (object)$request->profil;
//        $rumah = (object)$request->rumah;
//        $sertifikasi = (object)$request->sertifikasi;
//
//        $ptk = Ptk::create([
//            'nuptk' => $kepegawaian->nuptk,
//            'nama_lengkap' => $kepegawaian->nama,
//            'gelar_depan' => $kepegawaian->gelarDepan,
//            'gelar_belakang' => $kepegawaian->gelarBelakang,
//            'nip_lama' => $kepegawaian->nipLama,
//            'nip_baru' => $kepegawaian->nipBaru,
//            'id_jenis_pegawai' => $kepegawaian->jenisPegawai,
//            'id_sekolah' => $kepegawaian->unitKerja, // unit kerja (mistakenlly called id_sekolah lol xd)
//            'nik' => $profil->nik,
//            'nama_ibu' => $profil->namaIbu
//        ]);

        $ptk = new Ptk;
        return $ptk->store($request);

//        return $ptk->store((object) request()->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ptk $ptk
     * @return \Illuminate\Http\Response
     */
    public function show(Ptk $ptk)
    {
        return new PtkResource($ptk);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ptk $ptk
     * @return \Illuminate\Http\Response
     */
    public function edit(Ptk $ptk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Ptk $ptk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ptk $ptk)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ptk $ptk
     * @return \Illuminate\Http\Response
     */
    public function destroy($ptk)
    {
        $d = Ptk::destroy($ptk);
        return ['success' => true];
    }
}

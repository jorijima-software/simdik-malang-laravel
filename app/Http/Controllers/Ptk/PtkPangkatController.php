<?php

namespace App\Http\Controllers\Ptk;

use App\Http\Controllers\Controller;
use App\Http\Resources\Ptk\PangkatCollection;
use \App\Model\Ptk\PtkPangkat;
use Illuminate\Http\Request;

class PtkPangkatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($nip)
    {
        return PangkatCollection::collection(PtkPangkat::where('nip', $nip));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PtkPangkat  $ptkPangkat
     * @return \Illuminate\Http\Response
     */
    public function show(PtkPangkat $ptkPangkat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PtkPangkat  $ptkPangkat
     * @return \Illuminate\Http\Response
     */
    public function edit(PtkPangkat $ptkPangkat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PtkPangkat  $ptkPangkat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PtkPangkat $ptkPangkat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PtkPangkat  $ptkPangkat
     * @return \Illuminate\Http\Response
     */
    public function destroy(PtkPangkat $ptkPangkat)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Ptk;

use App\Http\Controllers\Controller;
use App\Http\Resources\Ptk\SekolahCollection;
use Illuminate\Http\Request;
use \App\Model\Ptk\PtkSekolah as PtkSekolahModel;

class PtkSekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ptk)
    {
        return SekolahCollection::collection(PtkSekolahModel::where('id_data_guru', $ptk)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PtkSekolah  $ptkSekolah
     * @return \Illuminate\Http\Response
     */
    public function show(PtkSekolah $ptkSekolah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PtkSekolah  $ptkSekolah
     * @return \Illuminate\Http\Response
     */
    public function edit(PtkSekolah $ptkSekolah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PtkSekolah  $ptkSekolah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PtkSekolah $ptkSekolah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PtkSekolah  $ptkSekolah
     * @return \Illuminate\Http\Response
     */
    public function destroy(PtkSekolah $ptkSekolah)
    {
        //
    }
}

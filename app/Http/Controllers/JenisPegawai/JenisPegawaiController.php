<?php

namespace App\Http\Controllers\JenisPegawai;

use App\Http\Controllers\Controller;
use App\JenisPegawai;
use Illuminate\Http\Request;

class JenisPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return JenisPegawai::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JenisPegawai  $jenisPegawai
     * @return \Illuminate\Http\Response
     */
    public function show(JenisPegawai $jenisPegawai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JenisPegawai  $jenisPegawai
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisPegawai $jenisPegawai)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JenisPegawai  $jenisPegawai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JenisPegawai $jenisPegawai)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JenisPegawai  $jenisPegawai
     * @return \Illuminate\Http\Response
     */
    public function destroy(JenisPegawai $jenisPegawai)
    {
        //
    }
}

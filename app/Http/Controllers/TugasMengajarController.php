<?php

namespace App\Http\Controllers;

use App\TugasMengajar;
use Illuminate\Http\Request;

class TugasMengajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TugasMengajar  $tugasMengajar
     * @return \Illuminate\Http\Response
     */
    public function show(TugasMengajar $tugasMengajar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TugasMengajar  $tugasMengajar
     * @return \Illuminate\Http\Response
     */
    public function edit(TugasMengajar $tugasMengajar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TugasMengajar  $tugasMengajar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TugasMengajar $tugasMengajar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TugasMengajar  $tugasMengajar
     * @return \Illuminate\Http\Response
     */
    public function destroy(TugasMengajar $tugasMengajar)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\TingkatSekolah;
use Illuminate\Http\Request;

class TingkatSekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TingkatSekolah  $tingkatSekolah
     * @return \Illuminate\Http\Response
     */
    public function show(TingkatSekolah $tingkatSekolah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TingkatSekolah  $tingkatSekolah
     * @return \Illuminate\Http\Response
     */
    public function edit(TingkatSekolah $tingkatSekolah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TingkatSekolah  $tingkatSekolah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TingkatSekolah $tingkatSekolah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TingkatSekolah  $tingkatSekolah
     * @return \Illuminate\Http\Response
     */
    public function destroy(TingkatSekolah $tingkatSekolah)
    {
        //
    }
}

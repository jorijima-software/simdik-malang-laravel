<?php

namespace App\Http\Controllers;

use App\TahunPelajaran;
use Illuminate\Http\Request;

class TahunPelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TahunPelajaran  $tahunPelajaran
     * @return \Illuminate\Http\Response
     */
    public function show(TahunPelajaran $tahunPelajaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TahunPelajaran  $tahunPelajaran
     * @return \Illuminate\Http\Response
     */
    public function edit(TahunPelajaran $tahunPelajaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TahunPelajaran  $tahunPelajaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TahunPelajaran $tahunPelajaran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TahunPelajaran  $tahunPelajaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(TahunPelajaran $tahunPelajaran)
    {
        //
    }
}

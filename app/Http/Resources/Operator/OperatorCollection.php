<?php

namespace App\Http\Resources\Operator;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class OperatorCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        Carbon::setLocale('id');
        return [
            'id' => $this->id,
            'npsn' => (!is_null($this->sekolah)) ? $this->sekolah->npsn : null,
            'namaSekolah' => (!is_null($this->sekolah)) ? $this->sekolah->nama : null,
            'namaOperator' => $this->nama_operator,
            'noHp' => $this->no_hp,
            'noWa' => $this->no_wa,
            'kodeVerifikasi' => $this->verify,
            'lastLogin' => (!is_null($this->sekolah) && !is_null($this->sekolah->log->last())) ? Carbon::parse($this->sekolah->log->last()->time)->diffForHumans() : null
        ];
    }
}

<?php

namespace App\Http\Resources\Berita;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class BeritaResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($res)
    {
        return [
            'id' => $this->id,
            'author' => $this->author->username,
            'title' => $this->title,
            'body' => $this->body,
            'createdAt' => $this->created_at->toDayDateTimeString()
        ];
    }
}

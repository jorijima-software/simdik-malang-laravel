<?php

namespace App\Http\Resources\Berita;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class BeritaCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($req)
    {
        Carbon::setLocale('id');
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => $this->author->username,
            'createdAt' => $this->created_at->diffForHumans()
        ];
    }
}

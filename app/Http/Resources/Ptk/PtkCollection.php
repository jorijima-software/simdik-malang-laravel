<?php

namespace App\Http\Resources\Ptk;


use Illuminate\Http\Resources\Json\Resource;

class PtkCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nuptk' => $this->nuptk,
            'nama' => $this->nama_lengkap,
            'count' => [
                'sekolah' => $this->sekolah->count(),
                'pangkat' => $this->pangkat->count(),
                'gaji' => $this->gaji->count()
            ],
            'unitKerja' => ($this->unitKerja !== null) ? $this->unitKerja->nama : null,
            'guru' => !!($this->guru !== null),
            'sertifikasi' => !!($this->sertifikasi && strlen($this->sertifikasi->no_peserta) > 0),
            'jenisPegawai' => !is_null($this->jenisPegawai) ? $this->jenisPegawai->nama: null,
            'statusPns' => $this->status_pns
        ];
    }
}

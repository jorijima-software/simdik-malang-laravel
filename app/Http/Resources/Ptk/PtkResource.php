<?php

namespace App\Http\Resources\Ptk;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class PtkResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        Carbon::setLocale('id');
        return [
            'id' => $this->id,
            'kepegawaian' => [
                'nuptk' => $this->nuptk,
                'nama' => $this->nama_lengkap,
                'jenisPegawai' => !is_null($this->jenisPegawai) ? $this->jenisPegawai->nama : null,
                'idJenisPegawai' => $this->id_jenis_pegawai,
                'unitKerja' => !is_null($this->unitKerja) ? $this->unitKerja->nama : null,
                'nip' => [
                    'lama' => $this->nip_lama,
                    'baru' => $this->nip_baru
                ],
                'gelar' => [
                    'depan' => $this->gelar_depan,
                    'belakang' => $this->gelar_belakang
                ]
            ],
            'profil' => !is_null($this->profil) ? [
                'nik' => $this->nik,
                'namaIbu' => $this->nama_ibu,
                'lahir' => [
                    'tanggal' => Carbon::parse($this->profil->tgl_lahir)->format('d/m/Y'),
                    'tempat' => $this->profil->tempat_lahir
                ],
                'agama' => !is_null($this->profil->agama) ? $this->profil->agama->nama : null
            ] : (object) [],
            'rumah' => !is_null($this->rumah) ? [
                'kota' => $this->rumah->kota,
                'kecamatan' => $this->rumah->kec,
                'alamat' => $this->rumah->alamat_rumah,
                'noTelp' => $this->rumah->nohp
            ] : (object) [],
            'sertifikasi' => !is_null($this->sertifikasi) ? [
                'pola' => $this->sertifikasi->pola_sertifikasi,
                'noPeserta' => $this->sertifikasi->no_peserta,
                'tahun' => $this->sertifikasi->tahun_sertifikasi,
                'bidangStudi' => ($bs = $this->sertifikasi->bidangStudi) ? $bs->kode . ' - ' . $bs->nama : null
            ] : (object) []
        ];
    }
}

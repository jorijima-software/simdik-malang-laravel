<?php

namespace App\Http\Resources\Ptk;

use Illuminate\Http\Resources\Json\Resource;

class PangkatCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'noSk' => $this->no_sk,
            'tmt' => $this->tmt,
            'unitKerja' => (is_null($this->id_sekolah)) ? $this->unit_kerja : $this->unitKerja->nama,
            'jenis' => [
                'golongan' => $this->golongan->gol,
                'ruang' => $this->golongan->ruang
            ],
            'kota' => (!is_null($this->id_kota)) ? $this->kota->name : null,
            'type' => $this->type
        ];
    }
}

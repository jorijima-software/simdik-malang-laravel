<?php

namespace App\Http\Resources\Ptk;

use Illuminate\Http\Resources\Json\Resource;

class SekolahCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama_sekolah,
            'tingkatPendidikan' => !is_null($this->tingkatPendidikan) ? $this->tingkatPendidikan->nama: null,
            'jurusan' => $this->jurusan,
            'kota' => $this->kota_sekolah,
            'kecamatan' => $this->kec_sekolah,
            'tahun' => [
                'masuk' => $this->thn_masuk,
                'lulus' => $this->thn_lulus
            ],
            'status' => $this->status
        ];
    }
}

<?php

namespace App\Http\Resources\Ptk;

use Illuminate\Http\Resources\Json\Resource;

class GajiCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nip' => $this->nip,
            'tmt' => $this->tmt,
            'noSk' => $this->no_sk,
            'jumlahGaji' => $this->jumlah_gaji,
            'jenis' => (!is_null($this->id_pangkat_golongan)) ? [
                'golongan' => $this->golongan->gol,
                'ruang' => $this->golongan->ruang
            ] : [],
        ];
    }
}

<?php

namespace App\Http\Resources\Guru;

use Illuminate\Http\Resources\Json\Resource;

class Collection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nuptk' => (!is_null($this->ptk)) ? $this->ptk->nuptk : null,
            'nama' => (!is_null($this->ptk)) ? $this->ptk->nama_lengkap : null,
            'jenisPegawai' => (!is_null($this->ptk)) ? $this->ptk->jenisPegawai->nama : null,
            'jumlahJam' => (!is_null($this->jamMengajar->first())) ? $this->jamMengajar->first()->jumlah_jam : 0,
            'tugasMengajar' => (!is_null($this->mataPelajaran)) ? $this->mataPelajaran->mataPelajaran->nama : null,
        ];
    }
}

<?php

namespace App\Http\Resources\Rombel;

use Illuminate\Http\Resources\Json\Resource;

class RombelCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'npsn' => $this->npsn,
            'nama' => $this->nama,
            'kecamatan' => $this->kecamatan->nama,
            'jumlahRombel' => $this->rombel->count(),
            'laki' => $this->rombel->sum('laki'),
            'perempuan' => $this->rombel->sum('perempuan'),
            'total' => $this->rombel->sum(function($row) {
                return $row->laki + $row->perempuan;
            }),
        ];
    }
}

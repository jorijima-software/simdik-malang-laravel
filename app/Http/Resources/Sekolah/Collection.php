<?php

namespace App\Http\Resources\Sekolah;


use Illuminate\Http\Resources\Json\Resource;

class Collection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'npsn' => $this->npsn,
            'nama' => $this->nama,
            'tingkatSekolah' => $this->tingkatSekolah->nama,
            'kecamatan' => $this->kecamatan->nama,
            'totalPegawai' => $this->ptk->count(),
            'persentase' => $this->persentase->count() / $this->ptk->count() * 100
        ];
    }
}

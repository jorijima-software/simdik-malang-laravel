<?php

namespace App\Http\Resources\Sekolah;

use Illuminate\Http\Resources\Json\Resource;

class SekolahResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        return [
            'id' => $this->id,
            'npsn' => $this->npsn,
            'nama' => $this->nama,
            'tingkatSekolah' => $this->tingkatSekolah->nama,
            'kecamatan' => $this->kecamatan->nama,
            'desa' => $this->desa,
            'alamat' => $this->alamat,
            'noTelp' => $this->noTelp,
            'faximile' => $this->fax,
            'email' => $this->email,
            'website' => $this->website,
            'status' => $this->status,
            'geografis' => $this->geografis,
            'image' => $this->picture
        ];
    }
}

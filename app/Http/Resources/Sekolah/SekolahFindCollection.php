<?php

namespace App\Http\Resources\Sekolah;

use Illuminate\Http\Resources\Json\Resource;

class SekolahFindCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama
        ];
    }
}

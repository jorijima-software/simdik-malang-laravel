<?php

namespace App;

use App\Model\Area\KecamatanMalang;
use App\Model\Log\Login;
use App\Model\Operator\Operator;
use App\Model\Ptk\RumahPtk;
use App\Model\Rombel\Rombel;
use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    protected $table = 'data_sekolah';

    public function tingkatSekolah()
    {
        return $this->hasOne(TingkatSekolah::class, 'id', 'id_jenis_tingkat_sekolah');
    }

    public function kecamatan()
    {
        return $this->hasOne(KecamatanMalang::class, 'id', 'id_kecamatan');
    }

    public function stats()
    {

    }

    public function ptk()
    {
        return $this->hasMany(Ptk::class, 'id_sekolah', 'id');
    }

    public function persentase()
    {
        return $this
            ->hasMany(Ptk::class, 'id_sekolah', 'id')
            ->whereRaw('LENGTH(nik) > 0');
    }

    public function log()
    {
        return $this->hasMany(Login::class, 'username', 'npsn');
    }

    public function operator()
    {
        return $this->hasOne(Operator::class, 'asal_sekolah', 'id');
    }

    public function percentageStats()
    {
        return $this
            ->with('ptk')
            ->selectRaw('id_jenis_tingkat_sekolah, COUNT(*) as total')
            ->orderBy('id_jenis_tingkat_sekolah', 'ASC')
            ->whereHas('ptk', function ($query) {
                $query
                    ->whereRaw('LENGTH(nik) > 0')
                    ->whereHas('rumah', function ($q) {
                        $q
                            ->whereRaw('LENGTH(alamat_rumah) > 0')
                            ->whereRaw('LENGTH(nohp) > 0');
                    });
            })
            ->groupBy('id_jenis_tingkat_sekolah')
            ->get();
    }

    public function countPersentase()
    {
        $stats = $this->percentageStats();
        $real = $this
            ->selectRaw('id_jenis_tingkat_sekolah, COUNT(*) as total')
            ->orderBy('id_jenis_tingkat_sekolah', 'ASC')
            ->groupBy('id_jenis_tingkat_sekolah')
            ->get();

        $return = [];
        foreach ($stats as $key => $row) {
            $return[] = [
                'tingkat' => TingkatSekolah::find($real[$key]['id_jenis_tingkat_sekolah'])->nama,
                'total' => $real[$key]['total'],
                'complete' => $row['total'],
                'percent' => round($row['total'] / $real[$key]['total'] * 100, 2),
            ];
        }
        return $return;
    }

    public function rombel()
    {
        return $this
            ->hasMany(Rombel::class, 'id_sekolah', 'id')
            ->where('id_tapel', TahunPelajaran::where('active', 1)->first()->id);
    }
}

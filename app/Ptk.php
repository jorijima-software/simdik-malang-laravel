<?php

namespace App;

use App\Model\Area\Kecamatan;
use App\Model\Area\Kota;
use App\Model\Guru\Guru;
use App\Model\Ptk\ProfilPtk;
use App\Model\Ptk\PtkGaji;
use App\Model\Ptk\PtkPangkat;
use App\Model\Ptk\RumahPtk;
use App\Model\Ptk\SertifikasiPtk;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Model\Ptk\PtkSekolah;

class Ptk extends Model
{
    protected $table = 'data_guru';
    protected $guarded = [];
    public $timestamps = false;

    public function store($request)
    {
        // this thing is inserting into multiple table so keep that in mind
        $kepegawaian = (object)$request->kepegawaian;
        $profil = (object)$request->profil;
        $rumah = (object)$request->rumah;
        $sertifikasi = (object)$request->sertifikasi;

        $insert_kp = $this::create([
            'nuptk' => $kepegawaian->nuptk,
            'nama_lengkap' => $kepegawaian->nama,
            'gelar_depan' => $kepegawaian->gelarDepan,
            'gelar_belakang' => $kepegawaian->gelarBelakang,
            'nip_lama' => $kepegawaian->nipLama,
            'nip_baru' => $kepegawaian->nipBaru,
            'id_jenis_pegawai' => $kepegawaian->jenisPegawai,
            'id_sekolah' => $kepegawaian->unitKerja, // unit kerja (mistakenlly called id_sekolah lol xd)
            'nik' => $profil->nik,
            'nama_ibu' => $profil->namaIbu,
            'status_pns' => in_array($kepegawaian->jenisPegawai, [3, 4, 5, 6])
        ]);

        $id_data_ptk = $insert_kp->id;

        ProfilPtk::create([
            'id_data_guru' => $id_data_ptk,
            'tempat_lahir' => $profil->tempatLahir,
            'tgl_lahir' => Carbon::parse($profil->tanggalLahir)->toDateTimeString(),
            'id_jenis_agama' => $profil->agama
        ]);

        RumahPtk::create([
            'id_data_guru' => $id_data_ptk,
            'alamat_rumah' => $rumah->alamat,
            'nohp' => $rumah->noTelp,
            'kota' => ($kota = Kota::find($rumah->kota)) ? $kota->name : null,
            'kec' => ($kecamatan = Kecamatan::find($rumah->kecamatan)) ? $kecamatan->name : null
        ]);

        /**
         * sertifikasi: {
         * pola: null,
         * noPeserta: null,
         * tahun: null,
         * bidangStudi: null
         * },
         */

        SertifikasiPtk::create([
            'id_data_guru' => $id_data_ptk,
            'pola_sertifikasi' => $sertifikasi->pola,
            'no_peserta' => $sertifikasi->noPeserta,
            'tahun_sertifikasi' => $sertifikasi->tahun,
            'id_bidang_studi' => $sertifikasi->bidangStudi
        ]);

        return ['success' => true];
    }

    public function show($filter, $specific = null)
    {
        $order = ['ascend' => 'ASC', 'descend' => 'DESC'];
        $column = ['nuptk' => 'nuptk', 'nama' => 'nama_lengkap'];
        return $this
            ->take(10)
            ->when(isset($specific), function ($query) use ($specific) {
                $sekolah = Sekolah::where('npsn', $specific)->first();
                $query->where('id_sekolah', $sekolah->id);
            })
            ->when(isset($filter['sortField']), function ($query) use ($filter, $order, $column) {
                $query->orderBy($column[$filter['sortField']], $order[$filter['sortOrder']]);
            })
            ->when(isset($filter['search']) && strlen($filter['search']) > 0, function ($query) use ($filter) {
                $search = urldecode($filter['search']);
                $query
                    ->where('nama_lengkap', 'like', '%' . $search . '%')
                    ->orWhere('nuptk', 'like', '%' . $search . '%')
                    ->orWhere('nip_baru', 'like', '%' . $search . '%')
                    ->orWhere('nip_lama', 'like', '%' . $search . '%');
            })
            ->when(isset($filter['nuptk']) && count($filter['nuptk']) < 2, function ($query) use ($filter) {
                foreach ($filter['nuptk'] as $nuptk) {
                    if ($nuptk == 1) {
                        $query->whereRaw('LENGTH(nuptk) > 0');
                    }
                    if ($nuptk == 0) {
                        $query->whereNull('nuptk');
                    }
                }
            })
            ->when(isset($filter['nama']), function ($query) use ($filter) {
                foreach ($filter['nama'] as $ft) {
                    if ($ft == 1) {
                        $query->whereHas('guru', function ($q) {
                            $q->whereRaw('id_data_guru');
                        });
                    }

                    if ($ft == 2) {
                        $query->whereHas('sertifikasi', function ($q) {
                            $q->whereRaw('LENGTH(no_peserta) > 0');
                        });
                    }
                }
            })->when(isset($filter['jenisPegawai']), function ($query) use ($filter) {
                $query->whereIn('id_jenis_pegawai', $filter['jenisPegawai']);
            })->when(isset($filter['unitKerja']) && count($filter['unitKerja']) < 2, function ($query) use ($filter) {
                foreach ($filter['unitKerja'] as $uk) {
                    if ($uk == 1) {
                        $query->whereHas('unitKerja', function ($q) {
                            $q->whereRaw('length(nama) > 0');
                        });
                    }
                    if ($uk == 0) {
                        $query->whereNull('id_sekolah');
                    }
                }
            });
    }

    public function sekolah()
    {
        return $this->hasMany(PtkSekolah::class, 'id_data_guru', 'id');
    }

    public function pangkat()
    {
        return $this->hasMany(PtkPangkat::class, 'nip', 'nip_baru');
    }

    public function gaji()
    {
        return $this->hasMany(PtkGaji::class, 'nip', 'nip_baru');
    }

    public function guru()
    {
        return $this->hasOne(Guru::class, 'id_data_guru', 'id');
    }

    public function unitKerja()
    {
        return $this->hasOne(Sekolah::class, 'id', 'id_sekolah');
    }

    public function jenisPegawai()
    {
        return $this->hasOne(JenisPegawai::class, 'id', 'id_jenis_pegawai');
    }

    public function rumah()
    {
        return $this->hasOne(RumahPtk::class, 'id_data_guru', 'id');
    }

    public function persentase($npsn)
    {
        return Ptk::with('rumah')
                ->where('id_sekolah', $npsn)
                ->whereHas('rumah', function ($q) {
                    $q
                        ->whereRaw('LENGTH(alamat_rumah) > 0')
                        ->whereRaw('LENGTH(nohp) > 0');
                })
                ->whereRaw('LENGTH(nik) > 0')
                ->count() / Ptk::where('id_sekolah', $npsn)->count() * 100;
    }

    public function profil()
    {
        return $this->hasOne(ProfilPtk::class, 'id_data_guru', 'id');
    }

    public function sertifikasi()
    {
        return $this->hasOne(SertifikasiPtk::class, 'id_data_guru', 'id');
    }

    public function persentasi()
    {
        return $this
            ->hasOne(RumahPtk::class)
            ->whereRaw('LENGTH(nohp) > 0');
    }
}

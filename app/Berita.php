<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';

    public function author() {
        return $this->hasOne(User::class, 'id', 'id_user');
    }

}

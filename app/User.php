<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user';

    public function sekolah() {
        return $this->hasOne(Sekolah::class, 'npsn', 'id_npsn');
    }

}

<?php

namespace App\Model\Rombel;

use App\Model\Log\Sekolah;
use App\TahunPelajaran;
use Illuminate\Database\Eloquent\Model;

class Rombel extends Model
{
    protected $table = 'rombel_sekolah';
//
//    public function sekolah()
//    {
//        return $this->belongsTo(Sekolah::class, 'id_sekolah', 'id');
//    }

    public function tahunPelajaran()
    {
        return $this->hasOne(TahunPelajaran::class, 'id', 'id_tapel');
    }


}

<?php

namespace App\Model\Area;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = 'regencies';
}

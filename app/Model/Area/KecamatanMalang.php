<?php

namespace App\Model\Area;

use Illuminate\Database\Eloquent\Model;

class KecamatanMalang extends Model
{
    protected $table = 'kecamatan';
}

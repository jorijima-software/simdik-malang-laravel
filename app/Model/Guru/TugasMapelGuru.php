<?php

namespace App\Model\Guru;

use App\Sekolah;
use App\Model\Guru\TugasMengajar;
use Illuminate\Database\Eloquent\Model;

class TugasMapelGuru extends Model
{
    protected $table = 'data_tugas_mapel';
    public $timestamps = false;
    protected $guarded = [];

    public function unitKerja() {
        return $this->hasOne(Sekolah::class, 'id', 'id_sekolah');
    }

    public function mataPelajaran() {
        return $this->hasOne(TugasMengajar::class, 'id', 'id_tugas_mengajar');
    }
}

<?php

namespace App\Model\Guru;

use App\Ptk;
use App\Semester;
use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'tugas_guru';

    public function jamMengajar() {
        return $this
            ->hasMany(TugasMapelGuru::class, 'id_tugas_guru', 'id')
            ->selectRaw('id_tugas_guru, SUM(jumlah_jam) jumlah_jam')
            ->where('semester', Semester::where('active', 1)->first()->id)
            ->groupBy('id_tugas_guru');
    }

    public function ptk() {
        return $this->hasOne(Ptk::class, 'id', 'id_data_guru');
    }

    public function mataPelajaran() {
        return $this
            ->hasOne(TugasMapelGuru::class, 'id_tugas_guru', 'id')
            ->where('semester', Semester::where('active', 1)->first()->id);
    }
}

<?php

namespace App\Model\Agama;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    protected $table = 'jenis_agama';
}

<?php

namespace App\Model\BidangStudi;

use Illuminate\Database\Eloquent\Model;

class BidangStudi extends Model
{
    protected $table = 'jenis_bidang_studi';
}

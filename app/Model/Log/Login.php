<?php

namespace App\Model\Log;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    protected $table = 'log_login';
}

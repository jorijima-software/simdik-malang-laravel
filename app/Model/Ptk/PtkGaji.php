<?php

namespace App\Model\Ptk;

use Illuminate\Database\Eloquent\Model;

class PtkGaji extends Model
{
    protected $table = 'gaji_berkala_guru';
    public $timestamps = false;
}

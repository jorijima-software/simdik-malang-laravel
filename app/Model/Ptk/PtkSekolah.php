<?php

namespace App\Model\Ptk;

use Illuminate\Database\Eloquent\Model;

class PtkSekolah extends Model
{
    protected $table = 'sekolah_guru';
    public $timestamps = false;

}

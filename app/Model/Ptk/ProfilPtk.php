<?php

namespace App\Model\Ptk;

use App\Model\Agama\Agama;
use Illuminate\Database\Eloquent\Model;

class ProfilPtk extends Model
{
    protected $table = 'profil_guru';
    protected $guarded = [];
    public $timestamps = false;

    public function agama() {
        return $this->hasOne(Agama::class, 'id', 'id_jenis_agama');
    }
}

<?php

namespace App\Model\Ptk;

use App\Model\BidangStudi\BidangStudi;
use Illuminate\Database\Eloquent\Model;

class SertifikasiPtk extends Model
{
    protected $table = 'sertifikasi_guru';
    protected $guarded = [];
    public $timestamps = false;

    public function bidangStudi() {
        return $this->hasOne(BidangStudi::class, 'id', 'id_bidang_studi');
    }

}

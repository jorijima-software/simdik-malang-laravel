<?php

namespace App\Model\Ptk;

use Illuminate\Database\Eloquent\Model;

class PtkPangkat extends Model
{
    protected $table = 'pangkat_guru';
    public $timestamps = false;
}
